new Vue({
    el: '#app',
    data: {
        rurning: false,
        playerLife: 100,
        monsterLife: 100,
        logs:[]
    },
    computed: {
        hasResult(){
            return this.playerLife == 0 || this.monsterLife == 0
        }
    },
    methods: {
        startGame(){
            this.rurning = true;
            this.playerLife = 100;
            this.monsterLife = 100;
            this.logs = [];
        },
        endGame(){
            this.rurning = false;
            this.playerLife = 100;
            this.monsterLife = 100;
            this.logs = [];
        },
        attack(especial) {
             this.hurt('monsterLife',5, 10, especial, 'Monstro', 'Jogador', 'monster');   
            if (this.monsterLife > 0) {
               this.hurt('playerLife', 7, 12, especial, 'Jogador', 'Monstro', 'player');
            }            
        },
        hurt(atr ,min, max, especial, source, large, cls) {
            const plus = especial ? 5 : 0;
            const hurt = this.getRandon(min + plus, max + plus);
            this[atr] = Math.max(this[atr] - hurt, 0);
            this.registerLog(`${source} atingil ${large} com ${hurt}.`, cls);
        },
        cureAndHurt() {
            this.cure(10, 15);
            this.hurt('playerLife',7, 12, false, 'Monstro', 'Jogador', 'monster');
        },
        cure(min, max) {
            const cure = this.getRandon(min , max);
            this.playerLife = Math.max(this.playerLife + cure , 100);
            this.registerLog(`Jogador ganho energia de ${cure}`, 'player');
        },
        getRandon(min, max) {
            const value = Math.random() * (max - min) + min;
            return Math.round(value);
        },
        registerLog(text, cls) {
            this.logs.unshift({ text, cls });
        }
    },
    watch: {
        hasResult(value) {
            if(value)this.rurning = false;
        }
    }
})